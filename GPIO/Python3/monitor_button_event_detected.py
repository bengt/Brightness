import time

import RPi.GPIO

RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setwarnings(False)
RPi.GPIO.setup(17, RPi.GPIO.OUT)
RPi.GPIO.setup(1, RPi.GPIO.IN)

RPi.GPIO.add_event_detect(1, RPi.GPIO.BOTH)

while True:
    for state in [RPi.GPIO.HIGH, RPi.GPIO.LOW]:
        while True:
            if RPi.GPIO.event_detected(1):
                RPi.GPIO.output(17, state)
                break
            time.sleep(0.1)
