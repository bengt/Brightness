import time

import wiringpi2

pin_base = 65   # arbitrary number above 64
i2c_addr = 0x21 # I2C address of the MCP23017
pin = 65        # 65 for GP01, 66 for GP02 and so on

wiringpi2.wiringPiSetup()
wiringpi2.mcp23017Setup(pin_base, i2c_addr)

wiringpi2.pinMode(pin, 1)  # 1 = output

while True:
   for state in [0, 1]:
       wiringpi2.digitalWrite(pin, state)
       time.sleep(1)
