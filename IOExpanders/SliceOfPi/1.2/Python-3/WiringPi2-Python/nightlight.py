import ephem

user = ephem.Observer()
user.lat = '53.143889'    # See wikipedia.org/Oldenburg
user.lon = '8.213889'     # See wikipedia.org/Oldenburg
user.elevation = 4        # See wikipedia.org/Oldenburg
user.temp = 20            # current air temperature gathered manually
user.pressure = 1019.5    # current air pressure gathered manually

day = user.next_rising(ephem.Sun()).datetime() >= user.next_setting(ephem.Sun()).datetime()

print("It's day." if day else "It's night.")
