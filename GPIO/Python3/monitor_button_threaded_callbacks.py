import time

import RPi.GPIO

RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setwarnings(False)
RPi.GPIO.setup(17, RPi.GPIO.OUT)
RPi.GPIO.setup(1, RPi.GPIO.IN)

def button_toggled ():
    RPi.GPIO.output(17, not RPi.GPIO.input(17))

RPi.GPIO.add_event_detect(1, RPi.GPIO.BOTH)
RPi.GPIO.add_event_callback(1, button_toggled)

while True:
    time.sleep(10)  # wait for RPi to call us back
