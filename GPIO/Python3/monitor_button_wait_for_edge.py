import time

import RPi.GPIO

RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setwarnings(False)
RPi.GPIO.setup(17, RPi.GPIO.OUT)
RPi.GPIO.setup(1, RPi.GPIO.IN)

while True:
    RPi.GPIO.wait_for_edge(1, RPi.GPIO.RISING)
    RPi.GPIO.output(17, RPi.GPIO.HIGH)
    RPi.GPIO.wait_for_edge(1, RPi.GPIO.FALLING)
    RPi.GPIO.output(17, RPi.GPIO.LOW)
