import time

import RPi.GPIO

RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setwarnings(False)
RPi.GPIO.setup(17, RPi.GPIO.OUT)

while True:
    RPi.GPIO.output(17, RPi.GPIO.HIGH)
    time.sleep(1)
    RPi.GPIO.output(17, RPi.GPIO.LOW)
    time.sleep(1)
