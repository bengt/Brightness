import time

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)
GPIO.setup(17, GPIO.OUT)

while True:
    time.sleep(1)
    print 0
    GPIO.output(17, GPIO.LOW)
    time.sleep(1)
    print 1
    GPIO.output(17, GPIO.HIGH)
