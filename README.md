## Brightness

Visual experiments with the Raspberry Pi. Enjoy the brightness.

## Challenges

For each programming language, output method (GPIO, IOExpander etc.) and library the following challenges should be implemented:

-   blink  
    (Toggle an LED every second)
-   blink all  
    (Toggle all LEDs every second)
-   strobe  
    (Activat an LED, deactivate an LED, repeat each second)
-   strobe all  
    (Activate all LEDs, deactivate all LEDS, repeat each second)
-   on  
    (Activate all LEDs)
-   off  
    (Deactivate all LEDs)
-   fade  
    (Oscillate the bightness of an LED with a frequency of 1 Hz)
-   fade all  
    (Oscillate the brightness of all LEDs with a frequency of 1 Hz)
-   run light  
    (Activate an LED, ascend in order each 100 ms, repeat)
-   snake  
    (Activate one LED every 200 ms in ascending order, deactivate one more LED every 200 ms in ascending order, repeat)
-   star trek  
    (Activate two LEDs mirrored at a center, ascend in order each 200 ms, repeat)
-   oscillate  
    (Activate one LED at a position oscillating with a frequency of 1 Hz)
-   ping status  
    (Activate some LEDs representing a fraction of the current ping to google.com and 100 ms)
-   ping visualize  
    (Send an ICMP_PING to www.google.com, activate an LED each 2 ms in ascending order, till an ICMP_ECHO is received, turn off all LEDs, repeat)
