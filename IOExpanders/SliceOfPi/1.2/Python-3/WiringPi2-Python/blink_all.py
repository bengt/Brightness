import time

import wiringpi2

pin_base = 65   # arbitrary number above 64
i2c_addr = 0x21 # I2C address of the MCP23017

wiringpi2.wiringPiSetup()
wiringpi2.mcp23017Setup(pin_base, i2c_addr)

states = [0, 1]
lights = range(0, 16)

# init
for light in lights:
    wiringpi2.pinMode(pin_base + light, 1)

# run
while True:
    for state in states:
        for light in lights:
            wiringpi2.digitalWrite(pin_base + light, state)
        time.sleep(1)
