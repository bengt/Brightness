import time

import RPi.GPIO

RPi.GPIO.setmode(RPi.GPIO.BCM)
RPi.GPIO.setwarnings(False)
RPi.GPIO.setup(17, RPi.GPIO.OUT)
RPi.GPIO.setup(1, RPi.GPIO.IN)

while True:
    if RPi.GPIO.input(1):
         RPi.GPIO.output(17, RPi.GPIO.HIGH)
    else:
        RPi.GPIO.output(17, RPi.GPIO.LOW)
